package com.pointcliki.dizgruntled.player;

import org.json.JSONObject;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.net.Player;
import com.pointcliki.core.ISavable;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.LevelScene;
import com.pointcliki.dizgruntled.LogicFactory;
import com.pointcliki.dizgruntled.logic.Grunt;

public class GruntzPlayer extends Player<Grunt> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5197346557565707011L;
	
	public static final String[] TEAMS = {"KING", "NAPOLEAN", "PATTON", "VIKING"};
	
	private int fClay = 0;
	private int fGruntMoulds = 0;
	private MegaphoneTray fTray;
	
	public GruntzPlayer(String fName) {
		super(fName);
		
		createMegaphoneTray();
	}
	
	protected void createMegaphoneTray() {
		
		fTray = new MegaphoneTray();
		
	}
	
	public MegaphoneTray megaphoneTray() {
		return fTray;
	}
	
	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public LevelScene levelScene() {
		return (LevelScene) playerManager().parent();
	}

	public void win() {
		
	}
	
	public void incrementClay() {
		if (fClay == 3) {
			fClay = 0;
			levelScene().hud().showClay(4);
			
			GruntzGame.resourceManager().playSound("GAME/SOUNDZ/GOOCOOKING1");
			levelScene().frameManager().queue(new Minion<FrameEvent>() {
				@Override
				public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
					fGruntMoulds++;
					GruntzGame.resourceManager().playSound("GAME/SOUNDZ/COOKINGCOMPLETE");
					return Minion.FINISH;
				}
			}, 200);
		} else {
			fClay++;
			levelScene().hud().showClay(fClay);
		}
	}
	
	public void dropGrunt(GridCoordinate xy) {
		if (fGruntMoulds > 0) {
			fGruntMoulds--;
			// Spawn grunt
			try {
				LogicFactory f = new LogicFactory();
				JSONObject obj = new JSONObject();
				obj.put("logic", "Grunt");
				obj.put("xy", xy.x() * 32 + " " + xy.y() * 32);
				obj.put("tool", "NORMAL");
				obj.put("toy", "NONE");
				// TODO: Fix to be correct for player
				obj.put("color", "ORANGE");
				obj.put("ai", "NO AI");
				obj.put("player", 0);
				Grunt g = (Grunt) f.createFromJSON(obj);
				levelScene().mapManager().map().placeLogic(g);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
