package com.pointcliki.dizgruntled.player;

import com.pointcliki.core.IManagerGroup;
import com.pointcliki.dizgruntled.logic.Grunt;
import com.pointcliki.net.PlayerManager;

public class GruntzPlayerManager extends PlayerManager<Grunt> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3929976622309614192L;

	public GruntzPlayerManager(IManagerGroup parent) {
		super(parent);
	}
	
	public GruntzPlayer player(String name) {
		return (GruntzPlayer) fPlayers.get(name);
	}
}

