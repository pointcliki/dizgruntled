package com.pointcliki.dizgruntled;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeSet;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;

import com.pointcliki.core.Manager;
import com.pointcliki.dizgruntled.map.TileSet;
import com.pointcliki.dizgruntled.rez.MonolithFile;
import com.pointcliki.dizgruntled.rez.MonolithPID;
import com.pointcliki.dizgruntled.rez.MonolithResource;
import com.pointcliki.dizgruntled.rez.MonolithWAV;
import com.pointcliki.dizgruntled.utils.GruntPalette;
import com.pointcliki.io.ResourceManager;

public class GruntzResourceManager extends ResourceManager {

	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = 1234234L;
	private static MonolithResource sRez;
	private static MonolithResource sVoicez;
	protected TileSet[] fTilesets;
	private static HashMap<Integer, String> fStrings;
	private static HashMap<String, TreeSet<String>> fTraits;
	private static HashMap<String, ToggleInfo> fToggle;
	protected HashMap<String, MonolithPID> fPIDs = new HashMap<String, MonolithPID>();
	private boolean levelInit = false;
	
	private static UnicodeFont sLargeFont;
	
	
	public GruntzResourceManager() {
		super("lib");
	}
	
	public MonolithResource rez() {
		if (sRez != null) return sRez;
		try {
			sRez = new MonolithResource("lib/GRUNTZ.REZ");
			return sRez;
		} catch (FileNotFoundException e) {
			System.err.println("Can't find GRUNTZ.REZ");
			JFrame frame = new JFrame();
			frame.setVisible(true);
			frame.setLocationRelativeTo(null);
			frame.setAlwaysOnTop(true);
			JOptionPane.showMessageDialog(frame, "Dizgruntled can't currently access the GRUNTZ.REZ file. Please check that it is not open in any other program.", "Fatal Error", 0);
			frame.dispose();
			System.exit(0);
			return null;
		}
	}
	
	public String textString(int num) {
		if (fStrings == null) {
			fStrings = new HashMap<Integer, String>();
			try {
				Scanner sc = new Scanner(new File("lib/en.txt"));
				while (sc.hasNextLine()) {
					String line = sc.nextLine();
					int index = Integer.parseInt(line.substring(0, 5));
					String text = line.substring(6);
					fStrings.put(index, text);
				}
				sc.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return fStrings.get(num);
	}
	
	public MonolithResource voicez() {
		if (sVoicez != null) return sVoicez;
		try {
			sVoicez = new MonolithResource("lib/GRUNTZ.VRZ");
			return sVoicez;
		} catch (FileNotFoundException e) {
			System.err.println("Can't find GRUNTZ.VRZ");
			return null;
		}
	}
	
	public MonolithPID pid(String ref) {
		if (!fPIDs.containsKey(ref)) {
			MonolithFile f = rez().file(ref, "pid");
			if (f == null) return null;
			fPIDs.put(ref, new MonolithPID(f));
		}
		return fPIDs.get(ref);
	}
	
	public MonolithPID pid(String ref, GruntPalette p) {
		if (!fPIDs.containsKey(ref + p.toString())) {
			MonolithPID img = pid(ref);
			if (img == null) return null;
			fPIDs.put(ref + p.toString(), new MonolithPID(p.colourize(img.image()), img.offset()));
		}
		return fPIDs.get(ref + p.toString());
	}
	
	public void initLevel() {
		if (levelInit) return;
		levelInit = true;
		
		if (sLargeFont == null) {
			try {
				sLargeFont = createLocalFont("lib/sundaycomicsbb_reg.ttf", 32);
			} catch (SlickException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// Load the tilesets
		File dir = new File("tilesets");

		String[] children = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".json");
			}
			
		});
		if (children != null) {
			fTilesets = new TileSet[children.length];
		    for (int i = 0; i < children.length; i++) fTilesets[i] = new TileSet(children[i]);
		}
		
		fTraits = new HashMap<String, TreeSet<String>>();
		// Load json content
		try {
			JSONObject j = new JSONObject(new JSONTokener(new FileReader("config/traits.json")));
			for (String s: JSONObject.getNames(j)) {
				String[] trs = j.getString(s).split(" ");
				TreeSet<String> set = new TreeSet<String>();
				for (String s2: trs) set.add(s2);
				fTraits.put(s, set);
			}
		} catch (FileNotFoundException e) {
			System.err.println("Couldn't find tile traits");
			System.err.println(e.getMessage());
		} catch (JSONException e) {
			System.err.println("Couldn't read tile traits");
			System.err.println(e.getMessage());
		}
		
		fToggle = new HashMap<String, ToggleInfo>();
		// Load json content
		try {
			JSONObject j = new JSONObject(new JSONTokener(new FileReader("config/toggle.json")));
			for (String s: JSONObject.getNames(j)) {
				String[] trs = j.getString(s).split(" ");
				if (trs.length == 1) fToggle.put(s, new ToggleInfo(trs[0], null, null, null));
				else if (trs.length == 3) fToggle.put(s, new ToggleInfo(trs[0], trs[1], trs[2], null));
				else if (trs.length == 5) fToggle.put(s, new ToggleInfo(trs[0], trs[1], trs[2], trs[3], trs[4]));
				else fToggle.put(s, new ToggleInfo(trs[0], trs[1], trs[2], trs[3]));
			}
		} catch (FileNotFoundException e) {
			System.err.println("Couldn't find tile toggles");
			System.err.println(e.getMessage());
		} catch (JSONException e) {
			System.err.println("Couldn't read tile toggles");
			System.err.println(e.getMessage());
		}
	}
	
	public UnicodeFont largeFont() {
		return sLargeFont;
	}
	
	public int tilesetCount() {
		return fTilesets.length;
	}
	
	public TileSet tileset(int i) {
		return fTilesets[i];
	}
	
	public TileSet tileset(String name) {
		for (TileSet s: fTilesets) if (s.name().equals(name)) return s;
		return null;
	}
	
	public TreeSet<String> traits(String tile) {
		if (fTraits.containsKey(tile)) return fTraits.get(tile);
		return new TreeSet<String>();
	}
	
	public ToggleInfo toggle(String tile) {
		return fToggle.get(tile);
	}
	
	@Override
	public void cleanup() {
		sRez.cleanup();
	}

	/**
	 * This method is not used as the manager is parented to the game, not scene
	 */
	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * This method is not used as the manager is parented to the game, not scene
	 */
	@Override
	public void restore(Manager from) {
		// TODO Auto-generated method stub
		
	}
	
	public static class ToggleInfo {
		private String fTile;
		private String fAni;
		private String fGraphics;
		private String fSound;
		private int fLayer = -1;
		
		public ToggleInfo(String tile, String animation, String graphics, String sound) {
			fTile = tile;
			fAni = animation;
			fGraphics = graphics;
			fSound = sound;
		}
		
		public ToggleInfo(String tile, String animation, String graphics, String sound, String layer) {
			this(tile, animation, graphics, sound);
			fLayer = Integer.parseInt(layer); 
		}

		public String tile() {
			return fTile;
		}
		public String animation(String area) {
			if (fAni == null) return fAni;
			return fAni.replace("area::", area + "/");
		}
		public String graphics(String area) {
			if (fGraphics == null) return fGraphics;
			return fGraphics.replace("area::", area + "/");
		}
		public String sound(String area) {
			if (fSound == null) return fSound;
			return fSound.replace("area::", area + "/");
		}
		public int layer() {
			return fLayer;
		}
	}

	public void playSound(String string) {
		if (!GruntzGame.instance().mute()) {
			new MonolithWAV(rez().file(string, "wav")).sound().play(1, 0.2f);
		}
	}
}

