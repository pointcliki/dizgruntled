package com.pointcliki.dizgruntled.utils;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class JImagePanel extends JPanel {

	private static final long serialVersionUID = -4152558470240797827L;
	
	private BufferedImage image;

	public JImagePanel(String file) {
		try {                
			image = ImageIO.read(new File(file));
			setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		} catch (IOException ex) {
			image = null;
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		if (image != null)
			g.drawImage(image, 0, 0, null);

	}
}