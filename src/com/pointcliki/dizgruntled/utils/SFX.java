package com.pointcliki.dizgruntled.utils;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.core.EntityContainer;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithFile;

public class SFX extends EntityContainer {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 6340292474497157526L;
	
	private AnimatedSprite fAnimation;
	private GridCoordinate fXY;
	private String fSound;
	private boolean fStale = false;
	
	public SFX(String animation, String graphics, String sound, GridCoordinate xy) {
		
		fXY = xy;
		MonolithFile f = GruntzGame.resourceManager().rez().file(animation, "ani");
		if (f == null) fAnimation = MonolithANI.fromDirectory(graphics);
		else fAnimation = new MonolithANI(f, graphics).sprite();
		fSound = sound;
		
		if (fAnimation == null) return;
		
		fAnimation.dispatcher().addMinion("animation.looped", new Minion<IEvent>() {
			@Override
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				SFX.this.cleanup();
				return Minion.FINISH;
			};
		});
		
		addChild(fAnimation);
	}
	
	public void init() {
		if (fAnimation != null) fAnimation.start();
		// Play sound
		if (fSound != null) GruntzGame.resourceManager().playSound(fSound);
	}
	
	@Override
	public void cleanup() {
		super.cleanup();
		fStale = true;
		fAnimation = null;
	}
	
	public boolean stale() {
		return fStale;
	}
	
	@Override
	public SFX position(Vector2f p) {
		super.position(new Vector2f(p.x + fXY.x() * 32 + 16, p.y + fXY.y() * 32 + 16));
		return this;
	}
}
