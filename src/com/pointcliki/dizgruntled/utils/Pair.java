package com.pointcliki.dizgruntled.utils;

import java.io.Serializable;

public class Pair implements Cloneable, Comparable<Object>, Serializable {
	/**
	 *  Serial Version UID
	 */
	private static final long serialVersionUID = -3302401777075738897L;
	
	int x;
	int y;
	
	public static Pair parseString(String string) {
		try {
			
			String[] array = string.split(" ");
			return new Pair(Integer.parseInt(array[0]), Integer.parseInt(array[1]));
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public Pair add(int addX, int addY) {
		return new Pair(x + addX, y + addY);
	}
	public Pair multiply(int factor) {
		return new Pair(x * factor, y * factor);
	}
	
	public Pair(int xs, int ys) {
		x = xs;
		y = ys;
	}
	
	public String toString() {
		return x + " " + y;
	}
	
	public int x() {
		return x;
	}
	
	public int y() {
		return y;
	}
	
	public void x(int xs) {
		x = xs;
	}
	
	public void y(int ys) {
		y = ys;
	}
	
	public int distanceTo(Pair xy) {
		return (int) Math.sqrt(Math.pow(xy.x - x, 2) + Math.pow(xy.y - y, 2));
	}
	
	public Pair clone() {
		return new Pair(x, y);
	}

	public boolean equals(Object o) {
		try {
			Pair pair = (Pair) o;
			
			if (x == pair.x && y == pair.y) return true;
			
		} catch (Exception e) {}
		return false;
	}

	public int compareTo(Object o) {
		
		if (o instanceof Pair) {
			Pair pair = (Pair) o;
			if (pair.x() != x || pair.y() != y) return 1;
			
		} else if (o instanceof String) {
			return compareTo(Pair.parseString((String) o));
		}
		
		return 0;
	}	
}
