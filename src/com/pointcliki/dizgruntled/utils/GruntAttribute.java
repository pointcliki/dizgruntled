package com.pointcliki.dizgruntled.utils;

public enum GruntAttribute {
	Selectable, Movable, Health, Speed
}
