package com.pointcliki.dizgruntled.editor;

import java.lang.reflect.InvocationTargetException;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.core.AnimatedSprite;

public class LogicSpriteFactory {
	
	public AnimatedSprite spriteFromJSON(JSONObject object) throws JSONException {
		Class<?> c;
		try {
			c = (Class<?>) Class.forName("com.pointcliki.dizgruntled.logic." + object.optString("logic"));
		} catch (ClassNotFoundException e) {
			try {
				c = (Class<?>) Class.forName(object.optString("logic"));
			} catch (ClassNotFoundException e2) {
				System.err.println("Can't find Java class for logic: " + object.optString("logic"));
				return null;
			}
		}
		try {
			Object o = c.getMethod("editorIcon", JSONObject.class).invoke(null, object);
			if (o instanceof AnimatedSprite) return (AnimatedSprite) o;
		} catch (IllegalAccessException e) {
			System.err.println(e.getMessage());
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
		} catch (InvocationTargetException e) {
			System.err.println(e.getMessage());
		} catch (NoSuchMethodException e) {
			System.err.println(e.getMessage());
		} catch (SecurityException e) {
			System.err.println(e.getMessage());
		}
		
		return null;
	}
}
