package com.pointcliki.dizgruntled.editor;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.input.MouseEvent;
import com.pointcliki.ui.DropDownMenu;
import com.pointcliki.ui.SelectionEvent;
import com.pointcliki.ui.UIEntity;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.map.TileSet;

public class SmartsSideBar extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5249065294785505726L;
	protected DropDownMenu fTilesetMenu;
	protected TileSet fTileset;
	protected int fSelectedTile;
	protected SpriteSheet fImages;

	public void init() {
		resize(new Vector2f(164, 740));
		
		fImages = PointClikiGame.resourceManager().spritesheet("editor/smarts", 32, 32);
		
		fTilesetMenu = new DropDownMenu(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				updateTiles();
				return Minion.CONTINUE;
			}
		});
		fTilesetMenu.resize(new Vector2f(155, 24));
		for (int i = 0; i < GruntzGame.resourceManager().tilesetCount(); i++) {
			TileSet set = GruntzGame.resourceManager().tileset(i);
			fTilesetMenu.add(set.name());
		}
		addChild(fTilesetMenu, 20);
		addChild(new Smarts(), 10);
		
		updateTiles();
	}

	private void updateTiles() {
		fTileset = GruntzGame.resourceManager().tileset(fTilesetMenu.selected());
	}
	
	public TileSet tileset() {
		return fTileset;
	}
	public int selected() {
		return fSelectedTile;
	}
	protected class Smarts extends UIEntity {

		/**
		 * Serial key
		 */
		private static final long serialVersionUID = 2352352367840040L;

		@Override
		public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
			super.handleUIMouseEvent(type, local, event);
			if (type.equals("mouse.down")) {
				int tile = (int) (Math.floor(local.x / 35) + Math.floor((local.y - 29) / 35) * 4);
				if (tile < 0 || tile >= 14) return;
				fSelectedTile = tile;
				scene(EditorScene.class).cursor().image(fTileset.tile(fSelectedTile - 1));
			}
		}
		
		@Override
		public boolean capturePoint(String type, Vector2f v, MouseEvent event) {
			if (v.x > 140 || (int) v.x % 35 > 31 || (int) (v.y - 29) % 35 > 31) return false;
			int tile = (int) (Math.floor(v.x / 35) + Math.floor((v.y - 29) / 35) * 4);
			if (tile < 0 || tile >= 14) return false;
			return true;
		}
		
		@Override
		public void render(Graphics graphics, long currentTime) {
			int count = fImages.getHorizontalCount();
			for (int i = 0; i < Math.min(count, 72); i++) {
				Image img = fImages.getSprite(i, 0);
				if (img != null) img.draw(i % 4 * 35, i / 4 * 35 + 29);
				if (fSelectedTile == i) {
					graphics.setColor(new Color(30, 80, 100));
					graphics.setDrawMode(Graphics.MODE_ADD);
					graphics.fillRect(i % 4 * 35, i / 4 * 35 + 29, 32, 32);
					graphics.setDrawMode(Graphics.MODE_NORMAL);
				}
			}
			PointClikiGame.resourceManager().UIFont().drawString(0, 660, "COMING SOON", Color.black);
			super.render(graphics, currentTime);
		}
	}
}