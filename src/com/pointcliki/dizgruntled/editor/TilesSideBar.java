package com.pointcliki.dizgruntled.editor;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.input.MouseEvent;
import com.pointcliki.ui.DropDownMenu;
import com.pointcliki.ui.ScrollBar;
import com.pointcliki.ui.SelectionEvent;
import com.pointcliki.ui.UIEntity;
import com.pointcliki.core.Entity;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.map.TileSet;

public class TilesSideBar extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -6545561788740272334L;
	
	protected DropDownMenu fTilesetMenu;
	protected ScrollBar fScrollbar;
	protected TileSet fTileset;
	protected int fSelectedTile;
	protected Entity fTiles;

	public void init() {
		resize(new Vector2f(164, 740));
		
		fTilesetMenu = new DropDownMenu(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				updateTiles();
				return Minion.CONTINUE;
			}
		});
		fTilesetMenu.resize(new Vector2f(155, 24));
		for (int i = 0; i < GruntzGame.resourceManager().tilesetCount(); i++) {
			TileSet set = GruntzGame.resourceManager().tileset(i);
			fTilesetMenu.add(set.name());
		}
		addChild(fTilesetMenu, 20);
		
		fTiles = new Tiles();
		addChild(fTiles, 10);
		
		fScrollbar = new ScrollBar(new Minion<SelectionEvent>(), true);
		fScrollbar.position(new Vector2f(142, 29));
		fScrollbar.resize(new Vector2f(12, 627));
		addChild(fScrollbar, 0);
		
		updateTiles();
	}

	private void updateTiles() {
		fTileset = GruntzGame.resourceManager().tileset(fTilesetMenu.selected());
		fSelectedTile = 0;
		fScrollbar.value(0);
		fScrollbar.ticks((int) Math.ceil((fTileset.count() + 1) / 4f));
		fScrollbar.barSpan(18);
	}
	
	public TileSet tileset() {
		return fTileset;
	}
	public int selected() {
		return fSelectedTile;
	}
	protected class Tiles extends UIEntity {

		/**
		 * Serial key
		 */
		private static final long serialVersionUID = 6109445697167840040L;

		@Override
		public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
			super.handleUIMouseEvent(type, local, event);
			if (type.equals("mouse.down")) {
				int tile = (int) (Math.floor(local.x / 35) + Math.floor((local.y - 29) / 35) * 4) + fScrollbar.value() * 4;
				if (tile < 0 || tile >= fTileset.count()) return;
				fSelectedTile = tile;
				scene(EditorScene.class).cursor().image(fTileset.tile(fSelectedTile - 1));
			}
		}
		
		@Override
		public boolean capturePoint(String type, Vector2f v, MouseEvent event) {
			if (v.x > 140 || (int) v.x % 35 > 31 || (int) (v.y - 29) % 35 > 31) return false;
			int tile = (int) (Math.floor(v.x / 35) + Math.floor((v.y - 29) / 35) * 4) + fScrollbar.value() * 4;
			if (tile < 0 || tile >= fTileset.count()) return false;
			return true;
		}
		
		@Override
		public void render(Graphics graphics, long currentTime) {
			int start = fScrollbar.value() * 4;
			int i = 1;
			if (start > 0) i = 0;
			int count = fTileset.count() - start + 1;
			for (; i < Math.min(count, 72); i++) {
				Image img = fTileset.tile(i + start - 1);
				if (img != null) img.draw(i % 4 * 35, i / 4 * 35 + 29);
				if (fSelectedTile == i + start) {
					graphics.setColor(new Color(30, 80, 100));
					graphics.setDrawMode(Graphics.MODE_ADD);
					graphics.fillRect(i % 4 * 35, i / 4 * 35 + 29, 32, 32);
					graphics.setDrawMode(Graphics.MODE_NORMAL);
				}
			}
			PointClikiGame.resourceManager().UIFont().drawString(0, 660, fTileset.type(fSelectedTile), Color.black);
			super.render(graphics, currentTime);
		}
	}
}