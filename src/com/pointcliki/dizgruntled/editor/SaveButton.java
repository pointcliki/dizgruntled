package com.pointcliki.dizgruntled.editor;

import com.pointcliki.core.Sprite;
import com.pointcliki.ui.UIEntity;


public class SaveButton extends UIEntity {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 6184459282941811191L;
	
	public SaveButton() {
		super();
		
		Sprite s = new Sprite("editor/save");
		addChild(s);
		fSpan = s.span();
	}
	
	@Override
	public void focus() {
		super.focus();
		scene(EditorScene.class).mapManager().save();
	}
}
