package com.pointcliki.dizgruntled;

import java.util.TreeMap;

import com.pointcliki.core.IManagerGroup;
import com.pointcliki.core.Manager;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.grid.GridManager;
import com.pointcliki.grid.GridManager.GridEvent;

/**
 * The pressure manager keeps track of how much force is being exerted on
 * tiles in the level. This is useful for knowing when to put switches down
 * 
 * @author hugheth
 * @version alpha-2.5
 * @since alpha-2
 * 
 * @see com.hugheth.dizgruntled.logic.system.StatefulLogic
 * @see com.hugheth.dizgruntled.manager.OldTimeManager.TimeManager
 * @see com.hugheth.dizgruntled.Level.Level
 */
public class PressureManager extends Manager {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -1779073720564436214L;
	
	private TreeMap<String, Byte> fPressure = new TreeMap<String, Byte>();
	
	public PressureManager(IManagerGroup g) {
		super(g);
	}
	
	// Pressure tile
	public void tilePressure(GridCoordinate xy) {
		
		if (fPressure.containsKey(xy.toString()))
			fPressure.put(xy.toString(), (byte) (fPressure.get(xy.toString()) + 1));
		else
			fPressure.put(xy.toString(), (byte) 1);
		
		fParent.manager(GridManager.class).dispatcher().dispatch(xy, "pressure", new PressureEvent(xy, fPressure.get(xy.toString())));
	}
	
	public void tileDepressure(GridCoordinate xy) {
		
		// Make sure key is immutable
		xy = xy.clone();
		
		if (fPressure.containsKey(xy.toString())) {
			fPressure.put(xy.toString(), (byte) Math.max(0, fPressure.get(xy.toString()) - 1));
		} else {
			fPressure.put(xy.toString(), (byte) 0);
		}
		
		fParent.manager(GridManager.class).dispatcher().dispatch(xy, "pressure", new PressureEvent(xy, fPressure.get(xy.toString())));
	}
	// Get pressure
	public int getTilePressureAt(GridCoordinate xy) {
		
		if (fPressure.containsKey(xy.toString())){
			return fPressure.get(xy.toString());
		} else
			return 0;
	}	
	
	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void restore(Manager from) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}
	
	public static class PressureEvent extends GridEvent {
		
		private GridCoordinate fXY;
		private int fPressure;
		
		public PressureEvent(GridCoordinate xy, int pressure) {
			fXY = xy;
			fPressure = pressure;
		}
		
		public GridCoordinate xy() {
			return fXY;
		}
		
		public int pressure() {
			return fPressure;
		}
	}
}