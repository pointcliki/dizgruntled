package com.pointcliki.dizgruntled.grunt;

public enum GruntState {
	IDLE, MOVING, DYING, PICKUP, WAITING_TO_MOVE, ACTING
}
