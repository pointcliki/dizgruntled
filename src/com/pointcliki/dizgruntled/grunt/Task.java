package com.pointcliki.dizgruntled.grunt;

import com.pointcliki.core.ISavable;
import com.pointcliki.dizgruntled.logic.Grunt;

public abstract class Task implements ISavable {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 7231664059253466650L;
	
	protected Grunt fGrunt;
	private boolean fCanceled = false;
	
	public Task(Grunt g) {
		fGrunt = g;
	}
	
	public abstract boolean next(); 
	
	public boolean start() {
		return true;
	}
	
	public void cancel() {
		fCanceled = true;
	}
	
	public boolean isCanceled() {
		return fCanceled;
	}

	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}
}
