package com.pointcliki.dizgruntled.rez;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

public class MonolithFile {

		protected RandomAccessFile fFile;
		private int fOffset;
		private int fLength;
		protected byte[] fData = null;

		public MonolithFile(RandomAccessFile file, String ext, int offset, int length) {
			fFile = file;
			fOffset = offset;
			fLength = length;
		}
		
		public MonolithFile(String s) {
			this(new File(s));
		}
		
		public MonolithFile(File f) {
			FileInputStream stream;
			try {
				stream = new FileInputStream(f);
				fData = new byte[(int) f.length()];
				stream.read(fData);
			} catch (IOException e1) {
			}
		}

		public byte[] data() {
			if (fData != null) return fData;
			try {
				fFile.seek(fOffset);
				byte[] buf = new byte[fLength];
				fFile.read(buf, 0, fLength);
				fData = buf;
				return fData;
			} catch (IOException e) {
				return null;
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public enum FileType {
			TEXT, PID
		}
}