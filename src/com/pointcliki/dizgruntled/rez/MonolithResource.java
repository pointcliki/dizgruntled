package com.pointcliki.dizgruntled.rez;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class MonolithResource extends MonolithDirectory {
	
	private boolean fLoaded = false;
	private String fHead;

	public MonolithResource(String ref) throws FileNotFoundException {
		super(new RandomAccessFile(ref, "r"), 0, 0);
		try {
			// Read initial properties
			init();
			// Loaded
			fLoaded = true;
		} catch (IOException e) {
			// Do nothing
		}
	}
	
	public boolean isLoaded() {
		return fLoaded;
	}
	
	public String header() {
		return fHead;
	}
	
	@Override
	public MonolithFile file(String ref, String ext) {
		if (ref.startsWith("mod::")) {
			File f = new File("mods/" + ref.substring(5) + "." + ext);
			if (f.exists()) return new MonolithFile(new File("mods/" + ref.substring(5) + "." + ext));
			return null;
		}
		
		String[] str = ref.split("/");
		if (str.length > 1) return resolve(str, 0);
		else return super.file(ref, ext);
	}
	
	@Override
	public MonolithDirectory directory(String ref) {
		String[] str = ref.split("/");
		if (str.length > 1) return resolveDirectory(str, 0);
		else return super.directory(ref);
	}

	@Override
	protected void init() throws IOException {
		byte[] head = new byte[127];
		fFile.read(head, 0, 127);
		fHead = new String(head);
		// Ditch version
		readInteger();
		// Node offset and size
		fOffset = readInteger();
		fSize = readInteger();
		
		super.init();
	}
	
	public void cleanup() {
		try {
			fFile.close();
			fChildDirs = null;
			fChildFiles = null;
		} catch (IOException e) {
			// Do nothing
		}
	}
	
	public static int readByte(byte[] data, int offset) {
		return 0xFF & data[offset];
	}

	public static int readHalf(byte[] data, int offset) {
		int a = readByte(data, offset);
		int b = readByte(data, offset + 1);
		return  a + (b << 8);
	}
	
	public static int readInteger(byte[] data, int offset) {
		int a = readByte(data, offset);
		int b = readByte(data, offset + 1);
		int c = readByte(data, offset + 2);
		int d = readByte(data, offset + 3);
		return  a + (b << 8) + (c << 16) + (d << 24);
	}
}