package com.pointcliki.dizgruntled.rez;

import java.util.ArrayList;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.utils.GruntPalette;

public class MonolithANI {

	protected MonolithFile fFile;
	protected String fDir;
	protected AnimatedSprite fAni;

	public MonolithANI(MonolithFile file, String source) {
		fFile = file;
		fDir = source;
	}
	
	public AnimatedSprite sprite() {
		return sprite(null);
	}
	
	public AnimatedSprite sprite(GruntPalette p) {
		if (fAni != null) return fAni;
		
		if (fFile == null) return null;
		
		// Parse info
		byte[] data = fFile.data();
		
		if (data == null) return null;
		int num = MonolithResource.readInteger(data, 12);
		Image[] images = new Image[num];
		Vector2f[] offsets = new Vector2f[num];
		int[] durs = new int[num];
		
		int n = MonolithResource.readInteger(data, 16) + 32;
		for (int i = 0; i < num; i++) {
			String index = MonolithResource.readHalf(data, n + 8) + "";
			while (index.length() < 3) index = "0" + index;
			
			MonolithPID pid;
			if (p == null) pid = GruntzGame.resourceManager().pid(fDir + "/FRAME" + index);
			else pid = GruntzGame.resourceManager().pid(fDir + "/FRAME" + index, p);
			
			if (pid != null) {
				images[i] = pid.image();
				offsets[i] = pid.offset();
				durs[i] = MonolithResource.readHalf(data, n + 10);
				// Check for effect
				int j = 0;
				if ((MonolithResource.readByte(data, n) & 2) == 2) {
					while (true) {
						int b = MonolithResource.readByte(data, n + 20 + j);
						j++;
						if (b == 0) break;
					}
				}
				n += 20 + j;
			}
		}
		fAni = new AnimatedSprite(images, offsets, durs) {
			/**
			 * Serial key
			 */
			private static final long serialVersionUID = 9001238884980035614L;

			@Override
			public String toString() {
				return "[Animation " + fDir + "]";
			}
		};
		return fAni;
	}
		
	public static AnimatedSprite fromDirectory(String directory) {
		return fromDirectory(directory, null);
	}

	public static AnimatedSprite fromDirectory(String directory, GruntPalette palette) {
		int j = 1;
		ArrayList<Image> images = new ArrayList<Image>();
		ArrayList<Vector2f> offsets = new ArrayList<Vector2f>();
		while (true) {
			String end = j + "";
			while (end.length() < 3) end = "0" + end;
			
			MonolithPID pid;
			if (palette == null) pid = GruntzGame.resourceManager().pid(directory  + "/FRAME" + end);
			else pid = GruntzGame.resourceManager().pid(directory  + "/FRAME" + end, palette);
			if (pid == null) break;
			images.add(pid.image());
			offsets.add(pid.offset());
			j++;
		}
		return new AnimatedSprite(images.toArray(new Image[images.size()]), offsets.toArray(new Vector2f[offsets.size()]));
	}
}

