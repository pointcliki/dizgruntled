package com.pointcliki.dizgruntled.rez;

import java.io.FileWriter;

import com.pointcliki.dizgruntled.GruntzGame;

public class MonolithTest {

	public static void main(String[] args) {
		try {
			MonolithResource res = new MonolithResource("GRUNTZ.REZ");
			
			GruntzGame.configureGame();
			
			MonolithWWD map = new MonolithWWD(res.file("AREA3/WORLDZ/LEVEL12", "wwd"));
						
			FileWriter w = new FileWriter("out.txt");
			w.write(new String(map.raw()));
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}