package com.pointcliki.dizgruntled.map;

import org.newdawn.slick.Graphics;

import com.pointcliki.core.Entity;

public class TileLayer extends Entity {
	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = 1L;
	
	private MapEntity fMapEntity;
	private int fLayer;
	
	public TileLayer(MapEntity me, int layer) {
		fMapEntity = me;
		fLayer = layer;
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		int sx = (int) Math.floor(-fMapEntity.offset().x / 32f);
		int sy = (int) Math.floor(-fMapEntity.offset().y / 32f);
		sx = Math.max(0, sx);
		sy = Math.max(0, sy);
		int w = Math.min(sx + fMapEntity.gridSpan().x(), fMapEntity.map().width());
		int h = Math.min(sy + fMapEntity.gridSpan().y(), fMapEntity.map().height());
		
		fMapEntity.map().tileset(fLayer).render(fMapEntity.map(), fLayer, sx, sy, w, h);
	}
}