package com.pointcliki.dizgruntled.map;

import com.pointcliki.dizgruntled.Logic;
import com.pointcliki.event.IEvent;

public class NewLogicEvent implements IEvent {

	private Logic fLogic;

	public NewLogicEvent(Logic l) {
		fLogic = l;
	}
	
	public Logic logic() {
		return fLogic;
	}
}
