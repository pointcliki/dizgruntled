package com.pointcliki.dizgruntled.map;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.ui.UIEntity;
import com.pointcliki.core.Entity;
import com.pointcliki.dizgruntled.Logic;
import com.pointcliki.dizgruntled.utils.SFX;

/**
 * @author Hugheth
 * @since alpha 2.5
 */
public class MapEntity extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 8817372315036130771L;
	private Map fMap;
	private Vector2f fOffset;
	private GridCoordinate fGridSpan;
	private UIEntity fLogicLayer;
	
	public MapEntity(Map map, Vector2f size) {
		super();
		
		fMap = map;
		fOffset = new Vector2f(0, 0);
		fLogicLayer = new LogicLayer(this);
		fLogicLayer.resize(size);
		resize(size);
		
		fMap.dispatcher().addMinion("newlogic", new Minion<IEvent>() {
			@Override
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				if (event instanceof NewLogicEvent) {
					Logic l = ((NewLogicEvent) event).logic();
					fLogicLayer.addChild(l);
					l.init(fMap);
				}
				return Minion.CONTINUE;
			}
		});
		
		fMap.dispatcher().addMinion("newsfx", new Minion<IEvent>() {
			@Override
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				if (event instanceof NewSFXEvent) {
					NewSFXEvent s = (NewSFXEvent) event;
					s.sfx().position(new Vector2f(fOffset.x, fOffset.y));
					MapEntity.this.addChild(s.sfx(), s.layer() * 100 + 50);
					s.sfx().init();
				}
				return Minion.CONTINUE;
			}
		});
	}
	
	public void init() {
		addChild(fLogicLayer, 1000);
		for (Logic l: fMap.logics()) {
			fLogicLayer.addChild(l);
			l.init(fMap);
		}
		for (int i = 0; i < fMap.tilesetCount(); i++) {
			addChild(new TileLayer(this, i).position(fOffset), i * 100);
		}
	}
	
	public GridCoordinate tileAtPoint(Vector2f v) {
		return new GridCoordinate((int) Math.floor((v.x - fOffset.x) / 32), (int) Math.floor((v.y - fOffset.y) / 32));
	}
	
	public Vector2f positionOfTile(GridCoordinate g) {
		return new Vector2f(g.x() * 32 + fOffset.x, g.y() * 32 + fOffset.y);
	}
	
	public void offset(Vector2f off) {
		fOffset = off;
		fLogicLayer.position(off);
		fLogicLayer.span().setX(-off.x);
		fLogicLayer.span().setY(-off.y);
		for (Entity e: fChildren) {
			if (e instanceof TileLayer) e.position(off);
			else if (e instanceof SFX) e.position(new Vector2f(off.x, off.y));
		}
	}
	
	public Vector2f offset() {
		return fOffset.copy();
	}
	
	@Override
	public MapEntity resize(Vector2f span) {
		super.resize(span);
		fGridSpan = new GridCoordinate(((int) Math.ceil(span.x / 32f)) + 1, ((int) Math.ceil(span.y / 32f)) + 1);
		return this;
	}
	
	public Map map() {
		return fMap;
	}
	
	public void hideLogics() {
		fLogicLayer.opacity(0);
	}
	public void showLogics() {
		fLogicLayer.opacity(1);
	}

	public void disableLogicEditing() {
		fLogicLayer.disable();
	}
	public void enableLogicEditing() {
		fLogicLayer.enable();
	}
	
	public GridCoordinate gridSpan() {
		return fGridSpan;
	}
	
	@Override
	public void cleanup() {
		fGridSpan = null;
		fMap.cleanup();
		fOffset = null;
		fLogicLayer.cleanup();
		super.cleanup();
	}
	
	@Override
	public String toString() {
		return "Map Entity [" + fMap.name() + "]";
	}
}
