package com.pointcliki.dizgruntled.map;

import com.pointcliki.dizgruntled.utils.SFX;
import com.pointcliki.event.IEvent;
import com.pointcliki.grid.GridCoordinate;

public class NewSFXEvent implements IEvent {

	private int fLayer;
	private SFX fSFX;
	private GridCoordinate fXY;

	public NewSFXEvent(int layer, SFX sfx, GridCoordinate xy) {
		fLayer = layer;
		fSFX = sfx;
		fXY = xy;
	}
	
	public int layer() {
		return fLayer;
	}
	
	public SFX sfx() {
		return fSFX;
	}
	public GridCoordinate xy() {
		return fXY;
	}
}
