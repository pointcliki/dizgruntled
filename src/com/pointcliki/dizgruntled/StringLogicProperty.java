package com.pointcliki.dizgruntled;

public abstract class StringLogicProperty extends LogicProperty {
	
	public StringLogicProperty(String name) {
		super(name);
	}

	/**
	 * @return The current value of the property 
	 */
	@Override
	public abstract String value();
	
	/**
	 * @return Set the current value of the property 
	 */
	public void value(String val) {};
	
	/**
	 * @return The possible choices for the property
	 */
	public abstract String[] choices(); 
	
	/**
	 * Set the property as the i choice
	 */
	public void choice(int i) {};
}
