package com.pointcliki.dizgruntled;

import java.io.File;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridManager;
import com.pointcliki.input.KeyEvent;
import com.pointcliki.net.PlayerManager;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.core.Scene;
import com.pointcliki.core.TimeManager.TimeEvent;
import com.pointcliki.dizgruntled.hud.HUD;
import com.pointcliki.dizgruntled.map.MapEntity;
import com.pointcliki.dizgruntled.map.GruntzMapManager;
import com.pointcliki.dizgruntled.player.GruntzPlayer;
import com.pointcliki.dizgruntled.player.GruntzPlayerManager;
import com.pointcliki.dizgruntled.player.HumanPlayer;
import com.pointcliki.dizgruntled.rez.MonolithFile;

/**
 * Scene that contains a Gruntz level
 * 
 * @author Hugheth
 * @since alpha 3
 */
public class LevelScene extends Scene {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -2909735438640087577L;
	protected GruntzPlayerManager fPlayerManager;
	protected HumanPlayer fHuman;
	protected GridManager fGridManager;
	protected GruntzMapManager fMapManager;
	private PressureManager fPressureManager;
	private EffectManager fEffectManager;
	
	private int fSW;
	private int fSH;
	
	private File fFile;
	private MonolithFile fMonolithFile;
	private HUD fHud;

	/**
	 * Create a new level scene
	 * @param game The game the scene is in
	 */
	public LevelScene(PointClikiGame game, File f) {
		super(game);
		registerUIMinion();
		fFile = f;
	}
	
	public LevelScene(PointClikiGame game, MonolithFile f) {
		super(game);
		registerUIMinion();
		fMonolithFile = f;
	}
	
	@Override
	public void init() {
		super.init();
		
		// Create human player
		fHuman = new HumanPlayer("human");
		fPlayerManager.add(fHuman);
		
		if (fFile != null) fMapManager.load(fFile);
		else fMapManager.importLevel(fMonolithFile);
		
		fSW = fGame.application().getWidth();
		fSH = fGame.application().getHeight();
								
		final MapEntity fMapViewer = new MapEntity(fMapManager.map(), new Vector2f(fGame.application().getScreenWidth(), fGame.application().getScreenHeight()));
		addChild(fMapViewer);
		fMapViewer.init();
		
		Minion<TimeEvent> scroller = new Minion<TimeEvent>() {
			public long run(com.pointcliki.event.Dispatcher<TimeEvent> dispatcher, String type, TimeEvent event) {
				if (GruntzGame.inputManager().isKeyPressed(Keyboard.KEY_RIGHT)) fMapViewer.offset(new Vector2f(Math.min(Math.max(fMapViewer.offset().x - event.delta() / 2f, -fMapViewer.map().width() * 32f + fSW), 0), fMapViewer.offset().y));
				else if (GruntzGame.inputManager().isKeyPressed(Keyboard.KEY_LEFT)) fMapViewer.offset(new Vector2f(Math.min(Math.max(fMapViewer.offset().x + event.delta() / 2f, -fMapViewer.map().width() * 32 + fSW), 0), fMapViewer.offset().y));
				if (GruntzGame.inputManager().isKeyPressed(Keyboard.KEY_DOWN)) fMapViewer.offset(new Vector2f(fMapViewer.offset().x, Math.min(Math.max(fMapViewer.offset().y - event.delta() / 2f, -fMapViewer.map().height() * 32 + fSH), 0)));
				else if (GruntzGame.inputManager().isKeyPressed(Keyboard.KEY_UP)) fMapViewer.offset(new Vector2f(fMapViewer.offset().x, Math.min(Math.max(fMapViewer.offset().y + event.delta() / 2f, -fMapViewer.map().height() * 32 + fSH), 0)));
				return Minion.CONTINUE;				
			};
		};
		timeManager().dispatcher().addMinion(TimeEvent.TYPE, scroller);
		
		// Escape
		GruntzGame.inputManager().keyDispatcher().addMinion("key.down", new Minion<KeyEvent>() {
			@Override
			public long run(Dispatcher<KeyEvent> dispatcher, String type, KeyEvent event) {
				if (event.key() == 1) {
					fHud.showMenu();
				}
				return Minion.CONTINUE;
			}
		});
		
		// HUD
		fHud = new HUD();
		addChild(fHud);
		
		timeManager().start();
	}
	
	public HUD hud() {
		return fHud;
	}
	
	public GruntzPlayer player(String name) {
		return fPlayerManager.player(name);
	}
	
	public HumanPlayer human() {
		return fHuman;
	}
	
	@Override
	public void configureManagers() {
		super.configureManagers();
		
		fPlayerManager = new GruntzPlayerManager(this);
		fGridManager = new GridManager(this, new Vector2f(32f, 32f), new Vector2f(16f, 16f));
		fMapManager = new GruntzMapManager(this);
		fPressureManager = new PressureManager(this);
		fEffectManager = new EffectManager(this);
		
		fManagers.put(PlayerManager.class, fPlayerManager);
		fManagers.put(GridManager.class, fGridManager);
		fManagers.put(GruntzMapManager.class, fMapManager);
		fManagers.put(PressureManager.class, fPressureManager);
		fManagers.put(EffectManager.class, fEffectManager);
	}
	
	public GruntzMapManager mapManager() {
		return fMapManager;
	}
	
	public GridManager gridManager() {
		return fGridManager;
	}
}
