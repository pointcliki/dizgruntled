package com.pointcliki.dizgruntled.task;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.grunt.Task;
import com.pointcliki.dizgruntled.logic.Grunt;
import com.pointcliki.dizgruntled.logic.Hidden;

public class HoleDigTask extends Task {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -3016688034160906270L;
	
	private GridCoordinate fTarget;

	public HoleDigTask(Grunt g, GridCoordinate xy) {
		super(g);
		fTarget = xy;
	}

	@Override
	public boolean next() {
		GruntzGame.resourceManager().playSound("GRUNTZ/SOUNDZ/SHOVELGRUNT/SHOVELGRUNTUI1");
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) {
					fGrunt.map().sfx(0, "GAME/ANIZ/DIRT", fGrunt.map().area(fTarget) + "/IMAGEZ/DIRT", null, fTarget);
				}
				return Minion.FINISH;
			}
		}, 15);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) {
					GruntzGame.resourceManager().playSound("GRUNTZ/SOUNDZ/SHOVELGRUNT/SHOVELGRUNTUI1");
				}
				return Minion.FINISH;
			}
		}, 35);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) {
					fGrunt.map().sfx(0, "GAME/ANIZ/DIRT", fGrunt.map().area(fTarget) + "/IMAGEZ/DIRT", null, fTarget);
				}
				return Minion.FINISH;
			}
		}, 50);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) {
					GruntzGame.resourceManager().playSound("GRUNTZ/SOUNDZ/SHOVELGRUNT/SHOVELGRUNTUI1");
				}
				return Minion.FINISH;
			}
		}, 70);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) {
					fGrunt.map().sfx(0, "GAME/ANIZ/DIRT", fGrunt.map().area(fTarget) + "/IMAGEZ/DIRT", null, fTarget);
				}
				return Minion.FINISH;
			}
		}, 85);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled())  {
					fGrunt.depleteStamina(5);
					fGrunt.map().tileToggle(fTarget);
					Hidden p = fGrunt.gridManager().getFirstEntityOfTypeAt(fTarget, Hidden.class);
					if (p != null) p.expose();
				}
				return Minion.FINISH;
			}
		}, 105);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) fGrunt.finishAction();
				return Minion.FINISH;
			}
		}, 120);
		
		return true;
	}
	
	@Override
	public boolean start() {
		if (!fTarget.adjacent(fGrunt.getTile())) return false;
		return true;
	}

}
