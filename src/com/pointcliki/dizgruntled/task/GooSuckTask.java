package com.pointcliki.dizgruntled.task;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.grunt.Task;
import com.pointcliki.dizgruntled.logic.Grunt;
import com.pointcliki.dizgruntled.logic.GruntPuddle;
import com.pointcliki.dizgruntled.modifier.AssignModifier;

public class GooSuckTask extends Task {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -3016688034160906270L;
	
	private GridCoordinate fTarget;
	private GruntPuddle fPuddle;
	private static AssignModifier<Boolean> sLock = new AssignModifier<Boolean>(50, false);
	
	public GooSuckTask(Grunt g, GridCoordinate xy) {
		super(g);
		fTarget = xy;
	}

	@Override
	public boolean next() {
		fPuddle = fGrunt.gridManager().getEntitiesOfTypeAt(fTarget, GruntPuddle.class).get(0);
		fPuddle.state(2);
		GruntzGame.resourceManager().playSound("GRUNTZ/SOUNDZ/GOOBERGRUNT/GOOBERGRUNTUI1");
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled())  {
					fGrunt.depleteStamina(5);
					fGrunt.attributeBoolean("controllable").addModifier(sLock);
					fPuddle.cleanup();
					
					// Add to clay
					fGrunt.player().incrementClay();
				}
				return Minion.FINISH;
			}
		}, 125);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) {
					fGrunt.finishAction();
					fGrunt.attributeBoolean("controllable").removeModifier(sLock);
				}
				
				return Minion.FINISH;
			}
		}, 240);
		
		return true;
	}
	
	@Override
	public void cancel() {
		super.cancel();
		fGrunt.attributeBoolean("controllable").removeModifier(sLock);
		fPuddle.state(1);
	}
	
	@Override
	public boolean start() {
		if (!fTarget.adjacent(fGrunt.getTile())) return false;
		return true;
	}

}
