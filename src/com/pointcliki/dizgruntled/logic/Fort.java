package com.pointcliki.dizgruntled.logic;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.GridLogic;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.LogicProperty;
import com.pointcliki.dizgruntled.StringLogicProperty;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.player.GruntzPlayer;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithPID;
import com.pointcliki.dizgruntled.rez.MonolithWWD;
import com.pointcliki.dizgruntled.utils.GruntPalette;
import com.pointcliki.grid.GridCoordinate;

/**
 * A logic to display the level fort
 * 
 * @author Hugheth
 * @since alpha 2.6
 */
public class Fort extends GridLogic {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5379476379535274956L;
	
	protected AnimatedSprite fAnimation;
	protected AnimatedSprite fWarlord;
	protected String fSource;
	protected String fArea;
	protected String fTeam;

	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		fSnapToGrid = true;
		super.importFromWWD(logic, image, animation, data);
		fSource = "internal";
		
		String[] s = image.split("/");
		fArea = s[0].substring(0, 5);
		fTeam = GruntzPlayer.TEAMS[MonolithWWD.readSmarts(data)];
		
		updateAnimation();
	}

	@Override
	public byte[] exportToWWD() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void importFromJSON(JSONObject object) {
		fSnapToGrid = true;
		super.importFromJSON(object);
		fSource = object.optString("source", "internal");
		fArea = object.optString("area");
		fTeam = object.optString("team", "KING");
		updateAnimation();
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject o = super.exportToJSON();
		o.put("area", fArea);
		if (!fTeam.equals("KING")) o.put("team", fTeam);
		if (!fSource.equals("internal")) o.put("source", fSource);
		return o;
	}
	
	public void init(Map map) {
		super.init(map);
		updateAnimation();
		fAnimation.start();
		fWarlord.start();
		
		if (!map().editing()) {
		
			// Add fort to grid
			gridManager().addObject(fTile, this);
			for (String s: GridCoordinate.COMPASS) {
				gridManager().addObject(fTile.add(GridCoordinate.fromString(s)), this);
			}
			for (String s: GridCoordinate.DIAGONAL_COMPASS) gridManager().addObject(fTile.add(GridCoordinate.fromString(s)), this);
		}
	}

	private void updateAnimation() {
		
		GruntPalette p = new GruntPalette("ORANGE", GruntzGame.resourceManager().rez().file("GRUNTZ/PALETTEZ/ORANGETOOL", "pal"));
		
		if (fAnimation != null) {
			fAnimation.cleanup();
			fWarlord.cleanup();
		}
		fAnimation = MonolithANI.fromDirectory(fArea + "/IMAGEZ/FORT");
		fWarlord = new MonolithANI(GruntzGame.resourceManager().rez().file("GRUNTZ/ANIZ/WARLORDZ/" + fTeam + "/MOVING", "ani"), "GRUNTZ/IMAGEZ/WARLORDZ/" + fTeam + "/MOVING").sprite(p); 
		addChild(fAnimation);
		addChild(fWarlord);
		fSpan = fAnimation.span();
		if (fMap != null) {
			fAnimation.start();
			fWarlord.start();
		}
	}

	@Override
	public void cleanup() {
		fAnimation.cleanup();
		super.cleanup();
	}

	@Override
	public String toString() {
		return "[Fort]";
	}

	@Override
	public void initProperties() {
		StringLogicProperty area = new StringLogicProperty("area") {
			
			@Override
			public String description() {
				return "The world which the fort comes from";
			}
			
			@Override
			public String value() {
				return fArea;
			}
			
			@Override
			public String[] choices() {
				return Map.WORLDS;
			}

			@Override
			public void choice(int i) {
				fArea = Map.AREAS[i];
				updateAnimation();
			}
		};
		StringLogicProperty team = new StringLogicProperty("team") {
			
			@Override
			public String description() {
				return "The team which the flag comes from";
			}
			
			@Override
			public String value() {
				return fTeam;
			}
			
			@Override
			public String[] choices() {
				return GruntzPlayer.TEAMS;
			}

			@Override
			public void choice(int i) {
				fTeam = GruntzPlayer.TEAMS[i];
				updateAnimation();
			}
		};
		fProperties = new LogicProperty[]{area, team};
	}
	
	public static AnimatedSprite editorIcon(JSONObject object) throws JSONException {
		MonolithPID pid = GruntzGame.resourceManager().pid(object.getString("area") + "/IMAGEZ/FORT/FRAME001");
		return new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
	}

	@Override
	public String name() {
		return "Fort";
	}
}
