package com.pointcliki.dizgruntled.logic;

import org.json.JSONException;
import org.json.JSONObject;

import com.pointcliki.core.Sprite;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.InvisibleLogic;
import com.pointcliki.dizgruntled.LogicProperty;
import com.pointcliki.dizgruntled.StringLogicProperty;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.rez.MonolithWWD;

public class Hidden extends InvisibleLogic {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 7956402676842575859L;
	
	private String fType;
	private String fItem;
	private String fSubType;
	private String fSubItem;
	private int fWWDTile = 0;
	private int fLayer;
	private String fNewTile = "DEF";
	
	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		super.importFromWWD(logic, image, animation, data);
		
		Sprite sprite = new Sprite(GruntzGame.resourceManager().spritesheet("editor/logics", 17, 17));
		if (logic.equals("Brickz"))
			sprite.subimage(2, 0);
		else {
			sprite.subimage(2, 1);
			
			fType = Pickup.typeForIndex(MonolithWWD.readPowerup(data));
			fItem = Pickup.itemForIndex(MonolithWWD.readPowerup(data));
			fWWDTile = MonolithWWD.readSmarts(data);
		}
		addChild(sprite);
	}

	@Override
	public byte[] exportToWWD() {
		return null;
	}

	@Override
	public void importFromJSON(JSONObject object) {
		super.importFromJSON(object);
		fType = object.optString("type");
		fItem = object.optString("item");
		fLayer = object.optInt("layer", 0);
		fNewTile = object.optString("tile", "DEF");
		
		Sprite sprite = new Sprite(GruntzGame.resourceManager().spritesheet("editor/logics", 17, 17));
		sprite.subimage(2, 1);
		addChild(sprite);
	}

	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject o = super.exportToJSON();
		o.put("type", fType);
		o.put("item", fItem);
		if (fLayer > 0) o.put("layer", fLayer);
		if (!fNewTile.equals("DEF")) o.put("tile", fNewTile);
		return o;
	}
	
	@Override
	public void init(Map map) {
		super.init(map);
		
		if (fWWDTile > 0) {
			// Find the tile layer that has this type
			for (int i = 0; i < map().tilesetCount(); i++) {
				byte b = map().tileset(i).wapValue(fWWDTile);
				if (b > 1) {
					fNewTile = map().tileset(i).type(b);
					fLayer = i;
					break;
				}
			}
		}
	}
	
	public void expose() {
		int tileid = map().tileset(fLayer).tile(fNewTile);
		map().tile(fLayer, fTile.x(), fTile.y(), tileid);
		
		if (!fType.equals(Pickup.NONE)) {
		
			// Create pickup
			Pickup p = new Pickup();
			try {
				p.importFromJSON(exportToJSON());
				map().placeLogic(p);
				
			} catch (Exception e) {
				System.err.println("Error creating pickup for hidden logic at " + fTile);
				System.err.println(e.getMessage());
			}
			
		}
			
		// No longer use the logic
		cleanup();
	}
	
	@Override
	public void initProperties() {
		StringLogicProperty type = new StringLogicProperty("type") {
			
			@Override
			public String description() {
				return "The type of pickup";
			}
			
			@Override
			public String value() {
				return fType;
			}
			
			@Override
			public String[] choices() {
				return Pickup.TYPES;
			}

			@Override
			public void choice(int i) {
				fType = Pickup.TYPES[i];
				if (fType.equals("TOOL")) fItem = "GAUNTLETZ"; 
				else if (fType.equals("TOY")) fItem = "BEACHBALL";
				else if (fType.equals("REWARD")) fItem = "COIN";
				else if (fType.equals("POWERUP")) fItem = "GHOST";
				else if (fType.equals("CURSE")) fItem = "SCREENSHAKE";
				else if (fType.equals("UTILITY")) fItem = "MEGAPHONEZ";
				initProperties();
			}
		};
		StringLogicProperty item = new StringLogicProperty("item") {
				
			@Override
			public String description() {
				return "The item to pickup";
			}
			
			@Override
			public String value() {
				return fItem;
			}
			
			@Override
			public String[] choices() {
				if (fType.equals("TOOL")) return Pickup.TOOLS;
				else if (fType.equals("TOY")) return Pickup.TOYS;
				else if (fType.equals("POWERUP")) return Pickup.POWERUPS;
				else if (fType.equals("CURSE")) return Pickup.CURSES;
				else if (fType.equals("REWARD")) return Pickup.REWARDS;
				else return Pickup.UTILITIES;
			}

			@Override
			public void choice(int i) {
				if (fType.equals("TOOL")) fItem = Pickup.TOOLS[i];
				else if (fType.equals("TOY")) fItem = Pickup.TOYS[i];
				else if (fType.equals("POWERUP")) fItem = Pickup.POWERUPS[i];
				else if (fType.equals("CURSE")) fItem = Pickup.CURSES[i];
				else if (fType.equals("REWARD")) fItem = Pickup.REWARDS[i];
				else fItem = Pickup.UTILITIES[i];
				initProperties();
			}
		};
		StringLogicProperty tileset = new StringLogicProperty("tile layer") {
			
			@Override
			public String description() {
				return "The layer index of the tile to change";
			}
			
			@Override
			public String value() {
				return fLayer + "";
			}
			
			@Override
			public String[] choices() {
				return null;
			}

			@Override
			public void value(String s) {
				try {
					fLayer = Integer.parseInt(s);
				} catch (Exception e) {
					fLayer = 0;
				}
			}
		};
		StringLogicProperty tile = new StringLogicProperty("tile type") {
			
			@Override
			public String description() {
				return "The type of the hidden tile";
			}
			
			@Override
			public String value() {
				return fNewTile;
			}
			
			@Override
			public String[] choices() {
				return null;
			}

			@Override
			public void value(String s) {
				fNewTile = s;
			}
		};
		if (fItem != null && fItem.equals("MEGAPHONEZ")) {
			
			StringLogicProperty subtype = new StringLogicProperty("subtype") {
				
				@Override
				public String description() {
					return "The sub item of pickup";
				}
				
				@Override
				public String value() {
					return fSubType;
				}
				
				@Override
				public String[] choices() {
					return Pickup.MEGAPHONE_TYPES;
				}

				@Override
				public void choice(int i) {
					fSubType = Pickup.MEGAPHONE_TYPES[i];
					initProperties();
				}
			};
			
			StringLogicProperty subitem = new StringLogicProperty("subitem") {
				
				@Override
				public String description() {
					return "The sub type of the pickup";
				}
				
				@Override
				public String value() {
					return fSubItem;
				}
				
				@Override
				public String[] choices() {
					if (fSubType.equals("TOOL")) return Pickup.TOOLS;
					else if (fSubType.equals("TOY")) return Pickup.TOYS;
					return Pickup.BRICK_TYPES;
				}

				@Override
				public void choice(int i) {
					if (fType.equals("TOOL")) fSubItem = Grunt.TOOLS[i];
					else if (fType.equals("TOY")) fSubItem = Grunt.TOYS[i];
					else fSubItem = Pickup.BRICK_TYPES[i];
					initProperties();
				}
			};
			fProperties = new LogicProperty[] {type, item, subtype, subitem, tileset, tile};
			return;
		}
		fProperties = new LogicProperty[] {type, item, tileset, tile};
	}

	@Override
	public String name() {
		return "Hidden";
	}
}

