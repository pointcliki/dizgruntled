package com.pointcliki.dizgruntled.logic;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.EffectManager.TriggerEvent;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.Logic;
import com.pointcliki.dizgruntled.LogicProperty;
import com.pointcliki.dizgruntled.StringLogicProperty;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithPID;
import com.pointcliki.dizgruntled.rez.MonolithWWD;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;

/**
 * A logic to display the level checkpoint flags
 * 
 * @author Hugheth
 * @since alpha 2.6
 */
public class Checkpoint extends Logic {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5379476379535274956L;
	
	private AnimatedSprite fAnimation;
	private boolean fReached = false;
	private int fTotalTriggers = 0;
	private String[] fGroups;

	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		super.importFromWWD(logic, image, animation, data);
		
		// Get groups
		ArrayList<String> groups = new ArrayList<String>();
		
		for (int i = 0; i < 24; i++) {
			int r = MonolithWWD.readRect(data, i);
			if (r > 0) groups.add("#" + r);
		}
				
		fGroups = groups.toArray(new String[groups.size()]);
		fTotalTriggers = fGroups.length;
		
		updateAnimation();
	}

	@Override
	public byte[] exportToWWD() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void importFromJSON(JSONObject object) {
		super.importFromJSON(object);
		fGroups = object.optString("groups", "").split(" ");
		if (fGroups.length == 1 && fGroups[0].equals("")) fGroups = new String[]{};
		fTotalTriggers = fGroups.length;
		updateAnimation();
	}
	
	public void init(Map map) {
		super.init(map);
		updateAnimation();
		fAnimation.start();
		
		if (!map.editing()) {
			
			Minion<TriggerEvent> minion = new Minion<TriggerEvent>() {
				@Override
				public long run(Dispatcher<TriggerEvent> dispatcher, String type, TriggerEvent event) {
					
					// Update total triggers to wait for
					if (event.on()) fTotalTriggers--;
					else fTotalTriggers++;
					
					if (fTotalTriggers == 0 && !fReached) {
						fReached = true;
						GruntzGame.resourceManager().playSound("GAME/SOUNDZ/FLAGRISE");
						updateAnimation();
					}
					
					return Minion.CONTINUE;
				}
			};
						
			// Iterate through groups and add the Trigger to the LogicManager
			for (String group: fGroups) effectManager().dispatcher().addMinion(group, minion);
		}
	}
	
	private String groupString() {
		if (fGroups.length == 0) return null;
		
		StringBuilder sb = new StringBuilder("");
		for (String s: fGroups) sb.append(s + " ");
		if (sb.length() == 0) return sb.toString();
		return sb.substring(0, sb.length() - 1);
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject o = super.exportToJSON();
		if (fGroups.length > 0) o.put("groups", groupString());
		return o;
	}

	private void updateAnimation() {
		AnimatedSprite sprite;
		if (fReached) {
			sprite = new MonolithANI(GruntzGame.resourceManager().rez().file("GAME/ANIZ/CHECKPOINTFLAGSET", "ani"), "GAME/IMAGEZ/CHECKPOINTFLAG").sprite();
		} else {
			MonolithPID pid = GruntzGame.resourceManager().pid("GAME/IMAGEZ/CHECKPOINTFLAG/FRAME001");
			sprite = new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
		}
		
		if (fAnimation != null) fAnimation.cleanup();
		fAnimation = sprite;
		addChild(fAnimation);
		fSpan = fAnimation.span();
		if (fMap != null) fAnimation.start();
	}
	

	@Override
	public void cleanup() {
		fAnimation.cleanup();
		super.cleanup();
	}

	@Override
	public String toString() {
		return "[Checkpoint]";
	}

	@Override
	public void initProperties() {
		StringLogicProperty group = new StringLogicProperty("group") {
			
			@Override
			public String description() {
				return "The group(s) of the effect";
			}
			
			@Override
			public String value() {
				return groupString();
			}
			
			@Override
			public String[] choices() {
				return null;
			}

			@Override
			public void value(String s) {
				fGroups = s.split(" ");					
			}
		};
		fProperties = new LogicProperty[]{group};
	}
	
	public static AnimatedSprite editorIcon(JSONObject object) throws JSONException {
		MonolithPID pid = GruntzGame.resourceManager().pid("GAME/IMAGEZ/CHECKPOINTFLAG/FRAME001");
		return new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
	}

	@Override
	public String name() {
		return "Checkpoint";
	}
}
