package com.pointcliki.dizgruntled.logic;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.GridLogic;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithPID;
import com.pointcliki.dizgruntled.rez.MonolithWWD;
import com.pointcliki.dizgruntled.utils.GruntPalette;

public class GruntPuddle extends GridLogic {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 7630965496790041953L;
	private AnimatedSprite fAnimation;
	private GruntPalette fPalette;
	private String fColor;
	
	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		fSnapToGrid = true;
		fFixed = -1000;
		super.importFromWWD(logic, image, animation, data);
		fColor = Grunt.COLORS[MonolithWWD.readSmarts(data)];
		state(1);
	}
	
	@Override
	public void importFromJSON(JSONObject object) {
		fSnapToGrid = true;
		fFixed = -1000;
		super.importFromJSON(object);
		fColor = object.optString("color", "ORANGE");
		state(1);
	}
	
	public void state(int s) {
		fPalette = new GruntPalette(fColor, GruntzGame.resourceManager().rez().file("GRUNTZ/PALETTEZ/" + fColor + "TOOL", "pal"));
		AnimatedSprite sprite = new MonolithANI(GruntzGame.resourceManager().rez().file("GRUNTZ/ANIZ/GRUNTPUDDLE/GRUNTPUDDLE" + (s + 1), "ani"), "GRUNTZ/IMAGEZ/GRUNTPUDDLE").sprite(fPalette);
		if (fAnimation != null) fAnimation.cleanup();
		fAnimation = sprite;
		addChild(fAnimation);
		if (map() != null) fAnimation.start();
		fSpan = fAnimation.span();
	}

	@Override
	public byte[] exportToWWD() {
		return null;
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = super.exportToJSON();
		obj.put("color", fColor);
		return obj;
	}
	
	@Override
	public void init(Map map) {
		super.init(map);
		fAnimation.start();	
	}

	@Override
	public void initProperties() {
		// TODO Auto-generated method stub

	}
	
	public static AnimatedSprite editorIcon(JSONObject object) throws JSONException {
		MonolithPID pid = GruntzGame.resourceManager().pid("GRUNTZ/IMAGEZ/GRUNTPUDDLE/FRAME001");
		return new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
	}

	@Override
	public String name() {
		return "GruntPuddle";
	}
}
