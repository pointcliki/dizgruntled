package com.pointcliki.dizgruntled.logic;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.GridLogic;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithPID;
import com.pointcliki.dizgruntled.rez.MonolithWWD;
import com.pointcliki.dizgruntled.utils.GruntPalette;
import com.pointcliki.input.MouseEvent;

public class GruntCreationPoint extends GridLogic {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 7630965496790041953L;
	private AnimatedSprite fAnimation;
	private GruntPalette fPalette;
	private String fColor;
	
	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		fSnapToGrid = true;
		fFixed = -1000;
		super.importFromWWD(logic, image, animation, data);
		fColor = Grunt.COLORS[MonolithWWD.readPoints(data)];
		setup();
	}
	
	@Override
	public void importFromJSON(JSONObject object) {
		fSnapToGrid = true;
		fFixed = -1000;
		super.importFromJSON(object);
		fColor = object.optString("color", "ORANGE");
		setup();
	}
	
	private void setup() {
		fPalette = new GruntPalette(fColor, GruntzGame.resourceManager().rez().file("GRUNTZ/PALETTEZ/" + fColor + "TOOL", "pal"));
		AnimatedSprite sprite = MonolithANI.fromDirectory("GAME/IMAGEZ/GRUNTCREATIONPOINT", fPalette);
		if (fAnimation != null) fAnimation.cleanup();
		fAnimation = sprite;
		addChild(fAnimation);
		if (map() != null) fAnimation.start();
		fSpan = fAnimation.span();
	}

	@Override
	public byte[] exportToWWD() {
		return null;
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = super.exportToJSON();
		obj.put("color", fColor);
		return obj;
	}
	
	@Override
	public void init(Map map) {
		super.init(map);
		fAnimation.start();
	}

	@Override
	public void initProperties() {
		// TODO Auto-generated method stub

	}
	
	public static AnimatedSprite editorIcon(JSONObject object) throws JSONException {
		MonolithPID pid = GruntzGame.resourceManager().pid("GAME/IMAGEZ/GRUNTCREATIONPOINT/FRAME001");
		return new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
	}

	@Override
	public String name() {
		return "GruntCreationPoint";
	}
	
	@Override
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		super.handleUIMouseEvent(type, local, event);
		if (type.equals("mouse.click")) {
			levelScene().human().dropGrunt(fTile);
		}
	}
}
