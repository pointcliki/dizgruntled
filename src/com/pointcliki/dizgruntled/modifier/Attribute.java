package com.pointcliki.dizgruntled.modifier;

import java.util.TreeSet;

public class Attribute<T> {
	
	private T fDefault;
	private TreeSet<Modifier<T>> fModifiers;
	
	public Attribute(T def) {
		fDefault = def;
		fModifiers = new TreeSet<Modifier<T>>();
	}
	
	public Attribute<T> addModifier(Modifier<T> m) {
		fModifiers.add(m);
		return this;
	}
	
	public Attribute<T> removeModifier(Modifier<T> m) {
		fModifiers.remove(m);
		return this;
	} 
	
	public T value() {
		T val = fDefault;
		for (Modifier<T> m: fModifiers) val = m.modify(val);
		return val;
	}
}
