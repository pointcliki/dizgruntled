package com.hugheth.dizgruntled.message;

import java.io.Serializable;

import org.newdawn.slick.Color;

/**
 * Message from a network player as a handshake
 * to create a new NetPlayer object or update an
 * already created one
 * @author Hugheth
 *
 */
public class HelloMessage implements Serializable {

	private static final long serialVersionUID = 2676456458189436685L;
	
	// A unique id to identify this player
	public long uid;
	
	public String playerName;
	public int[] playerColor;
	
	public HelloMessage(long u, String n, Color c) {
		uid = u;
		playerName = n;
		playerColor = new int[] {c.getRed(), c.getGreen(), c.getBlue()};
	}
	
	public Color getColor() {
		return new Color(playerColor[0], playerColor[1], playerColor[2]);
	}
}
