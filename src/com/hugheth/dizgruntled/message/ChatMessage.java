package com.hugheth.dizgruntled.message;

import java.io.Serializable;

import org.newdawn.slick.Color;

public class ChatMessage implements Serializable  {

	private static final long serialVersionUID = 3907884127427684886L;
	
	public long displayUntilFrame;
	public String message;
	public int[] color;
	
	public ChatMessage(String m, Color c, long f) {
		displayUntilFrame = f;
		message = m;
		color = new int[] {c.getRed(), c.getGreen(), c.getBlue()};
	}
}
